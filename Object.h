#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <math.h>

//#include "SFML/Graphics/Sprite.hpp"

#include "SFML/Graphics.hpp"

class CObject
{
    sf::Vector2f position;
    sf::Texture _texture;
    sf::Sprite sprite;
    int tileWidth;
    int tileHeight;
    int colSize;
    int index;
    int numTextures;
    sf::Vector2u spriteSize;
    sf::Vector2f scale;
    
public:
    CObject();

    void init();

    void loadTexture(std::string path, int x, int y, int w, int h);

    void loadTextureSheet(std::string path, int tileWidth, int tileHeight);

    void setTexture(int index, bool flip);

    void render(sf::RenderWindow &window, sf::Vector2f cameraPos);

    void getPosition(float &x, float &y);

    void setPosition(float x, float y);
    
    void addPosition(float x, float y);

    void getSpriteSize(float &x, float &y)
    {
        x = spriteSize.x;
        y = spriteSize.y;
    }

    void setSpriteSize(float x, float y);
};