#include "Game.h"

void CGame::setWindowSize(int width, int height)
{
    window.setSize(sf::Vector2u(width, height));
    updateAspectRatio(width, height);
}

/*
bool CGame::setTargetFramerate(double framerate)
{
    if(!running)
    {
        if(framerate > 0)
        {
            targetFramerate = framerate;
            return true;
        }
    }
    return false;
}*/

void CGame::setDebugState(bool state)
{
    debug = state;
}

void CGame::updateAspectRatio(int width, int height)
{
    window.setView(sf::View(sf::FloatRect(0.f, 0.f, width, height)));
}

void CGame::addObject(CObject obj)
{
    objectList.push_back(obj);
}

void CGame::gatherEvents(std::vector<BaseEvent*> &elist)
{
    while (window.pollEvent(tempEvent))
    {
        elist.push_back(Events::convertEvent(tempEvent));
    }
}

bool CGame::getKeyState(int key)
{
    return sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(key));
}

/*
void CGame::run()
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    running = true;
    std::chrono::steady_clock::time_point lastUpdate = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point lastFrame = std::chrono::steady_clock::now();
    double frameWait = targetFramerate <= 0 ? 0 : 1/targetFramerate;
    
    //Pre-define loop variables
    std::chrono::steady_clock::time_point ctime;
    std::list<CObject>::iterator loopVar;
    std::chrono::duration<double> deltaT;
    std::chrono::duration<double> deltaFrameT;
    sf::Event event;
    while(running)
    {
        //Get Events
        
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
                running = false;
            }
        }


        ctime = std::chrono::steady_clock::now();
        deltaT = std::chrono::duration_cast<std::chrono::duration<double>>(ctime - lastUpdate);

        if(level)
        {
            level.update(deltaT.count());
        }

        loopVar = objectList.begin();
        while(loopVar != objectList.end())
        {
            loopVar->update(deltaT.count());
            loopVar++;
        }

        deltaFrameT = std::chrono::duration_cast<std::chrono::duration<double>>(ctime - lastFrame);
        if(deltaFrameT.count() >= frameWait)
        {
            window.clear();

            if(level)
            {
                level.render(window, windowSize, cameraPos);
            }

            lastFrame = ctime;
            loopVar = objectList.begin();
            while(loopVar != objectList.end())
            {
                loopVar->render(window);
                loopVar++;
            }

            window.display();
        }
        lastUpdate = ctime;
        if(debug)
        {
            std::cout << "Tick Duration: " << deltaT.count() << std::endl;
            std::cout << "Frame Duration: " << deltaFrameT.count() << std::endl;
        }
    }
}*/