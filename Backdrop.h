#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Array2D.h"

#include "SFML/Graphics.hpp"

class CBackdrop
{
    std::vector<sf::Texture> textures;
    sf::Texture texture;
    sf::Vector2u rawTileSize;
    int countX;
    sf::Sprite sprite;
    sf::Vector2f tileSize;
    Array2D<int> tileMap;
    sf::Vector2u tileMapSize;
    bool wrap = false;
    sf::Vector2f offset;

public:

    CBackdrop(){offset = sf::Vector2f(0.0f, 0.0f);}

    void loadTextureSheet(std::string path, int tileWidth, int tileHeight);

    void loadTileMapFromFile(std::string path);

    void render(sf::RenderWindow &window, int windowWidth, int windowHeight, sf::Vector2f cameraPos);

    void setTileScale(float scalarX, float scalarY);
    
    void offsetPosition(float dx, float dy);
    
    void setWrap(bool state);

    void setTex();
};