#include "UIElement.h"

CUIElement::CUIElement()
{
    position = sf::Vector2f(0.0f, 0.0f);
    anchorPoint = sf::Vector2f(0.5f, 0.5f);
}

void CUIElement::update(double deltaT){}

void CUIElement::render(sf::RenderWindow &window){}

void CUIElement::getPosition(float &x, float &y)
{
    x = position.x;
    y = position.y;
}

void CUIElement::setPosition(float x, float y)
{
    position.x = x;
    position.y = y;
}

void CUIElement::addPosition(float x, float y)
{
    position.x += x;
    position.y += y;
}

void CUIElement::getAnchorPosition(float &x, float &y)
{
    x = anchorPoint.x;
    y = anchorPoint.y;
}

void CUIElement::setAnchorPosition(float x, float y)
{
    anchorPoint.x = x;
    anchorPoint.y = y;
}