# distutils: language = c++
# distutils: sources = [Object.cpp, Game.cpp, Events.cpp, Backdrop.cpp, UIElement.cpp, UITextElement.cpp, UISpriteElement.cpp]
# distutils: include_dirs = [SFML-2.4.2/include]
# distutils: library_dirs = [SFML-2.4.2/lib]
# distutils: libraries = [sfml-graphics, sfml-window, sfml-system]

cimport cython

from libcpp cimport bool
from libcpp.list cimport list
from libcpp.vector cimport vector
from libcpp.string cimport string

from cpython cimport array
import array

import time
import sys

from events cimport BaseEvent, KeyEvent, SizeEvent
from events cimport EventType, Key
from Side import Side

cdef extern from "UIElement.h":
    cdef cppclass CUIElement:
        CUIElement() except +
        void getPosition(float &x, float &y)
        void setPosition(float x, float y)
        void addPosition(float x, float y)
        void setAnchorPosition(float x, float y)
        void getAnchorPosition(float &x, float &y)

cdef extern from "UISpriteElement.h":
    cdef cppclass CUISpriteElement(CUIElement):
        CUISpriteElement() except +
        void loadTexture(string path, int x, int y, int w, int h)
        void loadTextureSheet(string path, int w, int h)
        void setTexture(int index, bool flip)
        void getSpriteSize(float &x, float &y)
        void setSpriteSize(float x, float y)

cdef extern from "UITextElement.h":
    cdef cppclass CUITextElement(CUIElement):
        CUITextElement() except +
        CUITextElement(string path) except +
        void loadFontFromFile(string path)
        string getText()
        void setText(string text)
        void getColour(int &R, int &G, int &B, int &A)
        void setColour(int R, int G, int B, int A)
        void getOutlineColour(int &R, int &G, int &B, int &A)
        void setOutlineColour(int R, int G, int B, int A)
        float getOutlineThickness()
        void setOutlineThickness(float size)
        int getCharacterSize()
        void setCharacterSize(int size)

cdef class UIElement:
    cdef CUIElement c_uielement
    cdef CUIElement* c_uielement_p        

    def getPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement.getPosition(x, y)
        return (x, y)

    def update(self, f):
        pass

    def prerender(self, f):
        pass

    def setPosition(self, x, y):
        self.c_uielement.setPosition(x, y)

    def addPosition(self, x, y):
        self.c_uielement.addPosition(x, y)

    def getAnchorPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement_p.getAnchorPosition(x, y)

    def setAnchorPosition(self, x, y):
        self.c_uielement_p.setAnchorPosition(x, y)

cdef class UITextElement(UIElement):
    cdef CUITextElement c_uitextelement
    visible = True

    def __cinit__(self, unicode path):
        self.c_uitextelement = CUITextElement(path.encode("utf-8"))
        self.c_uielement_p = &self.c_uitextelement

    property text:
        def __get__(self):
            return self.c_uitextelement.getText()
        def __set__(self, text):
            self.c_uitextelement.setText(text.encode("utf-8"))

    property characterSize:
        def __get__(self):
            return self.c_uitextelement.getCharacterSize()
        def __set__(self, size):
            self.c_uitextelement.setCharacterSize(size)

    property outlineThickness:
        def __get__(self):
            return self.c_uitextelement.getOutlineThickness()
        def __set__(self, size):
            self.c_uitextelement.setOutlineThickness(size)

    def getColour(self):
        cdef int r = 0
        cdef int g = 0
        cdef int b = 0
        cdef int a = 0
        self.c_uitextelement.getColour(r, g, b, a)
        return (r, g, b, a)

    def setColour(self, r, g, b, a=255):
        self.c_uitextelement.setColour(r, g, b, a)

    def getOutlineColour(self):
        cdef int r = 0
        cdef int g = 0
        cdef int b = 0
        cdef int a = 0
        self.c_uitextelement.getOutlineColour(r, g, b, a)
        return (r, g, b, a)

    def setOutlineColour(self, r, g, b, a=255):
        self.c_uitextelement.setOutlineColour(r, g, b, a)

    def getPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement_p.getPosition(x, y)
        return (x, y)

    def setPosition(self, x, y):
        self.c_uielement_p.setPosition(x, y)

    def addPosition(self, x, y):
        self.c_uielement_p.addPosition(x, y)

    def update(self, f):
        pass

    def prerender(self, f):
        pass

    def getAnchorPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement_p.getAnchorPosition(x, y)

    def setAnchorPosition(self, x, y):
        self.c_uielement_p.setAnchorPosition(x, y)


cdef class UISpriteElement(UIElement):
    cdef CUISpriteElement c_uispriteelement
    cdef list[float] spriteSize
    cdef list[float] size
    visible = True

    def __cinit__(self):
        self.c_uielement_p = &self.c_uispriteelement

    property spriteSize:
        def __get__(self):
            return self.spriteSize

    property size:
        def __get__(self):
            return self.size

    def setSpriteSize(self, x, y):
        self.size = [x, y]
        self.c_uispriteelement.setSpriteSize(x, y);
    
    """
    def loadTexture(self, path, posSize=[0, 0, 0, 0]):
        if(len(posSize) > 4):
            posSize = posSize[:4]
        elif(len(posSize) < 4):
            posSize.extend([0] * (4 - len(posSize)))
        self.c_uispriteelement.loadTexture(path.encode("utf-8"), posSize[0], posSize[1], posSize[2], posSize[3])
        cdef float x = 0;
        cdef float y = 0;
        self.c_uispriteelement.getSpriteSize(x, y)
        self.spriteSize = [x, y]
        self.size = [x, y]
    """

    def loadTexture(self, path):
        self.c_uispriteelement.loadTextureSheet(path.encode("utf-8"), -1, -1)
        self.c_uispriteelement.setTexture(0, False)
        cdef float x = 0;
        cdef float y = 0;
        self.c_uispriteelement.getSpriteSize(x, y)
        self.spriteSize = [x, y]
        self.size = [x, y]

    def loadTextureSheet(self, path, w, h):
        self.c_uispriteelement.loadTextureSheet(path.encode("utf-8"), w, h)
        self.spriteSize = [w, h]
        self.size = [w, h]

    def setTexture(self, index, flipped=False):
        self.c_uispriteelement.setTexture(index, flipped)
        
    def getPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement_p.getPosition(x, y)
        return (x, y)

    def setPosition(self, x, y):
        self.c_uielement_p.setPosition(x, y)

    def update(self, f):
        pass

    def prerender(self, f):
        pass

    def addPosition(self, x, y):
        self.c_uielement_p.addPosition(x, y)

    def getAnchorPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_uielement_p.getAnchorPosition(x, y)

    def setAnchorPosition(self, x, y):
        self.c_uielement_p.setAnchorPosition(x, y)

cdef extern from "Object.h":
    cdef cppclass CObject:
        CObject() except +
        void getPosition(float &x, float &y)
        void setPosition(float x, float y)
        void loadTexture(string path, int x, int y, int w, int h)
        void loadTextureSheet(string path, int w, int h)
        void setTexture(int index, bool flip)
        void addPosition(float x, float y)
        void getSpriteSize(float &x, float &y)
        void setSpriteSize(float x, float y)

cdef class Object:
    cdef CObject c_object
    cdef list[float] spriteSize
    cdef list[float] size
    visible = True
    collideable = True
    loadedTextures = 0

    def __cinit__(self):
        self.size = array.array("f", [0, 0])
        #self.c_object.setTest(self.update)
        pass#self.c_object = Object()

    property spriteSize:
        def __get__(self):
            return self.spriteSize

    property size:
        def __get__(self):
            return self.size

    property position:
        def __get__(self):
            cdef float x = 0
            cdef float y = 0
            self.c_object.getPosition(x, y)
            return (x, y)

    """
    def getPosition(self):
        cdef float x = 0
        cdef float y = 0
        self.c_object.getPosition(x, y)
        return (x, y)
    """

    def setPosition(self, x, y):
        self.c_object.setPosition(x, y)

    def setSpriteSize(self, x, y):
        self.size = [x, y]
        self.c_object.setSpriteSize(x, y)

    def loadTexture(self, path):
        self.c_object.loadTextureSheet(path.encode("utf-8"), -1, -1)
        self.c_object.setTexture(0, False)
        cdef float x = 0;
        cdef float y = 0;
        self.c_object.getSpriteSize(x, y)
        self.spriteSize = [x, y]
        self.size = [x, y]

    def loadTextureSheet(self, path, w, h):
        self.c_object.loadTextureSheet(path.encode("utf-8"), w, h)
        self.spriteSize = [w, h]
        self.size = [w, h]

    def setTexture(self, index, flipped=False):
        self.c_object.setTexture(index, flipped)

    def update(self, deltaT):
        pass

    def prerender(self, f):
        pass

    def addPosition(self, dx, dy):
        self.c_object.addPosition(dx, dy)

    cdef minkowski(self, pos1, size1, pos2, size2):
        cdef float w = 0.5 * (size1[0] + size2[0])
        cdef float h = 0.5 * (size1[1] + size2[1])
        cdef float dx = (pos1[0] + size1[0]/2) - (pos2[0] + size2[0]/2)
        cdef float dy = (pos1[1] + size1[1]/2) - (pos2[1] + size2[1]/2)
        cdef float wy = w * dy
        cdef float hx = h * dx

        if abs(dx) <= w and abs(dy) <= h:
            if wy > hx:
                if wy > -hx:
                    return Side.Top
                else:
                    return Side.Left
            else:
                if wy > -hx:
                    return Side.Right
                else:
                    return Side.Bottom
        else:
            return Side.Null
        #return not (pos1[0] >= pos2[0] + size2[0] or pos1[0] + size1[0] <= pos2[0] or pos1[1] >= pos2[1] + size2[1] or pos1[1] + size1[1] <= pos2[1])

    def isColliding(self, obj):
        return self.minkowski(self.position, self.size, obj.position, obj.size)

    def collide(self, other, side):
        pass


cdef extern from "Backdrop.h":
    cdef cppclass CBackdrop:
        CBackdrop() except +
        void loadTextureSheet(string path, int w, int h)
        void loadTileMapFromFile(string path)
        void setTileScale(float scalarX, float scalarY)
        void offsetPosition(float dx, float dy)
        void setWrap(bool state)

cdef class Backdrop:
    cdef CBackdrop c_terrain

    def loadTextureSheet(self, path, spriteWidth, spriteHeight):
        self.c_terrain.loadTextureSheet(path.encode("utf-8"), spriteWidth, spriteHeight)

    def loadTileMapFromFile(self, path):
        self.c_terrain.loadTileMapFromFile(path.encode("utf-8"))

    def setTileScale(self, x, y):
        self.c_terrain.setTileScale(x, y)

    def offsetPosition(self, dx, dy):
        self.c_terrain.offsetPosition(dx, dy)

    def setWrap(self, state):
        self.c_terrain.setWrap(state)


cdef extern from "Game.h":
    cdef cppclass CGame:
        CGame() except +
        double targetFramerate
        #bool setTargetFramerate(double framerate)
        void run()
        void addObject(CObject obj)
        void setWindowSize(int width, int height)
        void renderObject(CObject* obj)
        void renderUIElement(CUIElement *ele)
        void close()
        void gatherEvents(vector[BaseEvent*] &elist)
        void windowClear()
        void windowDisplay()
        void setWindowTitle(char* title)
        bool getKeyState(int key)
        void setCameraPosition(float x, float y)
        void updateAspectRatio(int width, int height)
        void renderLevel(CBackdrop* level, int windowWidth, int windowHeight)

cdef class Game:
    cdef CGame c_game
    cdef bool running
    objects = []
    uiElements = []
    cdef int[:] windowSize
    cdef float[:] cameraPos
    cdef vector[BaseEvent*] events
    registeredKeys = []
    keyHandlers = []
    keyDown = [False for x in range(0, 101)]
    #cdef public bool debug
    cdef public bool _debug
    debugLabel = UITextElement("arial_7.ttf")
    cdef public Backdrop level
    cdef public int _tickrate
    cdef public int _framerate
    cdef public bool _paused

    def __cinit__(self):
        self.windowSize = array.array("i", [200, 200])
        self.cameraPos = array.array("f", [0, 0])
        self.running = False
        self._debug = False
        self._tickrate = -1
        self._framerate = -1
        self.paused = False

    def __init__(self, title="Engine"):
        self.setWindowTitle(title)
        self.debugLabel.outlineThickness = 2
        self.debugLabel.setOutlineColour(0, 0, 0)
        self.debugLabel.characterSize = 16
        self.debugLabel.setAnchorPosition(0, 1)
        self.debugLabel.setPosition(0, 1)

    def setWindowTitle(self, title):
        self.c_game.setWindowTitle(title.encode("utf-8"))
 
    property windowSize:
        def __get__(self):
            return self.windowSize

    property running:
        def __get__(self):
            return self.running
        def __set__(self, b):
            self.running = b

    property paused:
        def __get__(self):
            return self._paused
        def __set__(self, b):
            self._paused = b

    property tickrate:
        def __get__(self):
            return self._tickrate
        def __set__(self, i):
            self._tickrate = i

    property framerate:
        def __get__(self):
            return self._framerate
        def __set__(self, i):
            self._framerate = i

    property cameraPosition:
        def __get__(self):
            return self.cameraPos

    property debug:
        def __get__(self):
            return self._debug
        def __set__(self, value):
            self._debug = value

    def setLevel(self, level):
        self.level = <Backdrop>level

    def setTargetFramerate(self, framerate):
        pass#self.c_game.setTargetFramerate(framerate)

    def addObject(self, Object obj):
        self.objects.append(obj)
        #self.c_game.addObject(obj.c_object)

    def addUIElement(self, UIElement ele):
        self.uiElements.append(ele)
        #self.c_game.addObject(obj.c_object)

    def setWindowSize(self, width, height):
        self.c_game.setWindowSize(width, height)
        self.windowSize = array.array('i', [width, height])

    def updateWindowSize(self, width, height):
        self.c_game.updateAspectRatio(width, height)
        self.windowSize = array.array('i', [width, height])

    def _setCameraPosition(self):
        self.c_game.setCameraPosition(self.cameraPos[0], self.cameraPos[1])

    def setCameraPosition(self, x, y):
        self.cameraPos = array.array('f', [x, y])
        self._setCameraPosition()

    def addCameraPosition(self, x, y):
        self.cameraPos[0] += x
        self.cameraPos[1] += y
        self._setCameraPosition()

    def getObjectScreenPositon(self, obj):
        xpos = obj.position[0] - self.cameraPos[0]
        ypos = obj.position[1] - self.cameraPos[1]
        return (xpos / self.windowSize[0], ypos / self.windowSize[1])

    def renderObject(self, Object obj):
        self.c_game.renderObject(&obj.c_object)

    def renderUIElement(self, UIElement ele):
        self.c_game.renderUIElement(ele.c_uielement_p)

    def renderLevel(self):
        self._renderLevel()

    cdef _renderLevel(self): 
        self.c_game.renderLevel(&self.level.c_terrain, self.windowSize[0], self.windowSize[1])

    def gatherEvents(self):
        self.events.clear()
        self.c_game.gatherEvents(self.events)

    def close(self):
        self.running = False
        self.c_game.close()

    def windowClear(self):
        self.c_game.windowClear()

    def windowDisplay(self):
        self.c_game.windowDisplay()

    def registerKey(self, index):
        if not index in self.registeredKeys:
            self.registeredKeys.append(index)

    def registerKeys(self, indexes):
        for i in indexes:
            self.registerKey(i)

    def registerKeyHandler(self, object):
        self.keyHandlers.append(object)

    def isKeyPressed(self, key):
        return self.c_game.getKeyState(key)

    def run(self):
        self._run()

    def clearObjects(self):
        self.objects.clear()

    cdef processKey(self, KeyEvent* e, bool keyDown):
        if e.code == Key.Return and keyDown:
            self.paused = not self.paused


    cdef _run(self):
        self.running = True
        lastUpdate = time.time()
        lastFrame = lastUpdate
        ctime = 0
        cdef int x = 0
        frameWait = 0
        cdef BaseEvent* e
        cdef KeyEvent* e2
        cdef SizeEvent* e3
        fdelta = 0
        if self.tickrate <= 0:
            tickwait = 0
        else:
            tickwait = 1 / self.tickrate

        if self.framerate <= 0:
            frameWait = 0
        else:
            frameWait = 1 / self.framerate

        while self.running:
            ctime = time.time()

            self.gatherEvents()
                
            for e in self.events:
                if e.type == EventType.Closed:
                    self.close()
                elif e.type == EventType.Resized:
                    e3 = <SizeEvent*>(e)
                    self.updateWindowSize(e3.width, e3.height)
                elif e.type == EventType.KeyPressed or e.type == EventType.KeyReleased:
                    e2 = <KeyEvent*>(e)
                    self.processKey(e2, e.type == EventType.KeyPressed)

            if ctime - lastUpdate < tickwait:
                time.sleep(tickwait - (ctime - lastUpdate))
            else:

                if self.debug:
                    fdelta = ctime-lastUpdate
                    tps = "TPS: {}".format(1/fdelta if fdelta > 0 else "Infinity")
                    sys.stdout.write("\r" + tps)
                    sys.stdout.flush()
                    self.debugLabel.text = tps

                if self.paused:
                    time.sleep(0.1)
                    lastUpdate = time.time()
                    continue

                onscreen = []

                for obj in self.objects:
                    obj.update(ctime-lastUpdate)
                    pos = self.getObjectScreenPositon(obj)
                    if not (pos[0] < -0.1 or pos[0] > 1.1 or pos[1] < -0.1 or pos[1] > 1.1):
                        if obj.collideable:
                            onscreen.append(obj)

                for x, obj1 in enumerate(onscreen[:-1]):
                    for y, obj2 in enumerate(onscreen[1:]):
                        if not obj1 is obj2:
                            if x < y + 1:
                                side = obj1.isColliding(obj2)
                                if not side == Side.Null:
                                    obj1.collide(obj2, side)
                                    obj2.collide(obj1, side.opposite())

                for ele in self.uiElements:
                    ele.update(ctime-lastUpdate)

                lastUpdate = ctime
                if ctime - lastFrame >= frameWait:
                    self.windowClear()
                    if not self.level is None:
                        self.renderLevel()

                    for obj in self.objects:
                        obj.prerender(ctime - lastFrame)
                        if obj.visible:
                            self.renderObject(obj)

                    for ele in self.uiElements:
                        ele.prerender(ctime - lastFrame)
                        if ele.visible:
                            self.renderUIElement(ele)
                    
                    if self.debug:
                        self.renderUIElement(self.debugLabel)

                    lastFrame = ctime
                    self.windowDisplay()

