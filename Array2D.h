#pragma once
#include <math.h>
#include <stdexcept>


//Lovely little 2D Array Wrapper I wrote
//Very interested to hear any issues with it
template <class T>
class Array2D
{
	//Essentially just a wrapper for a pointer array
	T *_data = nullptr;

protected:
	int width, height;

public:
	//These could not be constant, and should probably be getter functions.
	

	Array2D<T>(){}

	void setSize(int _width, int _height)
	{
		height = _height;
		width = _width;
		delete _data;
		_data = new T[_width * _height];
	}

	Array2D<T>(int _width, int _height) : height(_height), width(_width)
	{
		_data = new T[_width * _height];
	}
	~Array2D()
	{
		delete _data;
	}

	void set(int x, int y, T data)
	{
		//Sanity Check
		if (x >= width || x < 0 || y >= height || y < 0)
		{
			std::string err = "Index provided (" + std::to_string(x) + ", " + std::to_string(y) + ") is out of range (" + std::to_string(width) + ", " + std::to_string(height) + ")";
			throw std::out_of_range(err);
		}
		_data[y * width + x] = data;
	}

	T get(int x, int y)
	{
		//Sanity Check
		if (x >= width || x < 0 || y >= height || y < 0)
		{
			std::string err = "Index provided (" + std::to_string(x) + ", " + std::to_string(y) + ") is out of range (" + std::to_string(width) + ", " + std::to_string(height) + ")";
			throw std::out_of_range(err);
		}
		return _data[y * width + x];
	}
};