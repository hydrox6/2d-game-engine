from enum import IntEnum


class Side(IntEnum):
    Null = -1,
    Top = 0,
    Left = 1,
    Bottom = 2,
    Right = 3,
    Total = 4

    def opposite(self):
        if self == Side.Null:
            return Side.Null
        return Side((self + 2) % Side.Total)

    def __str__(self):
        return self.name
