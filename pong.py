import engine
import random, math
from Keys import Keys

game = engine.Game("Pong")
game.setWindowSize(800, 600)


class Paddle(engine.Object):

    def __init__(self, x, up_key, down_key):
        super().__init__()
        self.height: int = 160
        self.setPosition(x, (game.windowSize[1] / 2) - (self.height / 2))
        self.speed: float = 100
        self.loadTexture("data/spritesheets/ball.png")
        self.setSpriteSize(20, self.height)
        self.up_key = up_key
        self.down_key = down_key
        game.addObject(self)

    def update(self, f):
        if game.isKeyPressed(self.up_key):
            self.addPosition(0, -self.speed * f)
        if game.isKeyPressed(self.down_key):
            self.addPosition(0, self.speed * f)
        if self.position[1] < 0:
            self.setPosition(self.position[0], 0)
        elif self.position[1] > game.windowSize[1] - self.height:
            self.setPosition(self.position[0], game.windowSize[1] - self.height)


scoreLeft = engine.UITextElement("arial_7.ttf")
scoreLeft.text = "0"
scoreLeft.characterSize = 48
scoreLeft.setPosition(0.333333, 0)
scoreLeft.setAnchorPosition(0.5, 0)
scoreLeft.setOutlineColour(0, 0, 0)
scoreLeft.outlineThickness = 3
game.addUIElement(scoreLeft)

scoreRight = engine.UITextElement("arial_7.ttf")
scoreRight.text = "0"
scoreRight.characterSize = 48
scoreRight.setPosition(0.666667, 0)
scoreRight.setAnchorPosition(0.5, 0)
scoreRight.setOutlineColour(0, 0, 0)
scoreRight.outlineThickness = 3
game.addUIElement(scoreRight)

player1 = Paddle(40, Keys.W, Keys.S)
player2 = Paddle(game.windowSize[0] - 60, Keys.Up, Keys.Down)


class Ball(engine.Object):

    def __init__(self):
        super().__init__()
        self.speed: float = 0
        self.paused: bool = False
        self.base_speed: int = 80
        self.round_time: float = 0
        self.reset()
        self.direction = [random.choice([-1, 1]), random.choice([-1, 1])]
        self.loadTexture("data/spritesheets/ball.png")
        self.setSpriteSize(16, 16)
        game.addObject(self)

    def reset(self):
        self.setPosition(game.windowSize[0] / 2 - 8, game.windowSize[1] / 2 - 8)
        self.direction = [random.choice([-1, 1]), random.choice([-1, 1])]
        if self.base_speed < 140:
            self.base_speed += 10
        self.speed = self.base_speed
        self.paused = True

    def update(self, f):
        if not self.paused:
            self.addPosition(self.speed * f * self.direction[0], self.speed * f * self.direction[1])
            self.round_time += f
            if self.round_time >= 1:
                self.speed += math.log10(self.round_time * 160)
                self.round_time = 0
        elif game.isKeyPressed(Keys.Space):
            self.paused = False

        pos = self.position
        if pos[1] <= 0:
            self.setPosition(pos[0], 0)
            self.direction[1] = 1
        if pos[1] >= game.windowSize[1] - 16:
            self.setPosition(pos[0], game.windowSize[1] - 16)
            self.direction[1] = -1

        if pos[0] <= 0:
            scoreRight.text = str(int(scoreRight.text) + 1)
            self.reset()

        if pos[0] >= game.windowSize[0] - 16:
            scoreLeft.text = str(int(scoreLeft.text) + 1)
            self.reset()

        if not (pos[0] > player1.position[0] + player1.size[0] or pos[0] - 16 < player1.position[0]
                or pos[1] > player1.position[1] + player1.size[1] or pos[1] + 16 < player1.position[1]):
            self.direction[0] = 1
        if not (pos[0] + 16 < player2.position[0] or pos[0] > player2.position[0] + player2.size[0]
                or pos[1] > player2.position[1] + player2.size[1] or pos[1] + 16 < player2.position[1]):
            self.direction[0] = -1


ball = Ball()

game.run()
