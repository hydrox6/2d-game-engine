#include "Object.h"

CObject::CObject()
{
    init();
}

void CObject::init()
{
    position = sf::Vector2f(0.0f, 0.0f);

}

void CObject::loadTextureSheet(std::string path, int w, int h)
{
    _texture.loadFromFile(path);
    sf::Vector2u size = _texture.getSize();
    
    index = 0;

    int width = size.x;
    int height = size.y; 
    if((width <= w && height <= h) || (w == -1 && h == -1))
    {
        spriteSize = sf::Vector2u(width, height);
        numTextures = 1;
        colSize = 1;
        tileWidth = width;
        tileHeight = height;
        return;
    }
    spriteSize = sf::Vector2u(w, h);

    int countX = ceil(width / w);
    int countY = ceil(height / h);

    colSize = countX;
    tileWidth = w;
    tileHeight = h;

    numTextures = countY * countX;
    sprite.setTexture(_texture);
}

void CObject::setTexture(int i, bool flip)
{
    if(i < 0 || i >= numTextures)
    {
        std::cout << "Invalid Texture Index " << i << std::endl;
        return;
    }
    index = i;
    if(flip)
    {
        sprite.setTextureRect(sf::IntRect(((i+1) % colSize) * tileWidth, (i / colSize) * tileHeight, -tileWidth, tileHeight));
    }
    else
    {
        sprite.setTextureRect(sf::IntRect((i % colSize) * tileWidth, (i / colSize) * tileHeight, tileWidth, tileHeight));
    }
    sprite.setTexture(_texture);
}

void CObject::setSpriteSize(float x, float y)
{
    float scalarX = x / spriteSize.x;
    float scalarY = y / spriteSize.y;
    scale = sf::Vector2f(scalarX, scalarY);
    sprite.setScale(scalarX, scalarY);
}

void CObject::render(sf::RenderWindow &window, sf::Vector2f cameraPos)
{
    //std::cout << "Rendering an object" << std::endl;
    //std::cout << position.x << ", " << position.y << ", " << position.z << std::endl;
    //std::cout << "Render: " << test << std::endl;
    sprite.setPosition(position - cameraPos);
    window.draw(sprite);
}

void CObject::getPosition(float &x, float &y)
{
    x = position.x;
    y = position.y;
}

void CObject::setPosition(float x, float y)
{
    position.x = x;
    position.y = y;
}

void CObject::addPosition(float x, float y)
{
    position.x += x;
    position.y += y;
}