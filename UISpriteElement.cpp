#include "UISpriteElement.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System/Vector2.hpp"

CUISpriteElement::CUISpriteElement()
{
    position = sf::Vector2f(0.0f, 0.0f);
    scalar = sf::Vector2f(1.0f, 1.0f);
}

void CUISpriteElement::loadTexture(std::string path, int x, int y, int w, int h)
{
    sf::IntRect initialSpritePos(x, y, w, h);
    if (!_texture.loadFromFile(path))
    {
        std::cout << "Error loading sprite from " << path << std::endl;
    }
    sprite.setTexture(_texture);
    if(initialSpritePos != sf::IntRect())
    {
        sprite.setTextureRect(initialSpritePos);
        spriteSize = sf::Vector2u(initialSpritePos.width, initialSpritePos.height);
    }
    else
    {
        spriteSize = _texture.getSize();
    }
}

void CUISpriteElement::loadTextureSheet(std::string path, int w, int h)
{
    _texture.loadFromFile(path);
    sf::Vector2u size = _texture.getSize();
    
    index = 0;

    int width = size.x;
    int height = size.y; 
    if((width <= w && height <= h) || (w == -1 && h == -1))
    {
        spriteSize = sf::Vector2u(width, height);
        numTextures = 1;
        colSize = 1;
        tileWidth = width;
        tileHeight = height;
        return;
    }
    spriteSize = sf::Vector2u(w, h);

    int countX = ceil(width / w);
    int countY = ceil(height / h);

    colSize = countX;
    tileWidth = w;
    tileHeight = h;

    numTextures = countY * countX;
    sprite.setTexture(_texture);
}

void CUISpriteElement::setTexture(int i, bool flip)
{
    if(i < 0 || i >= numTextures)
    {
        std::cout << "Invalid Texture Index " << i << std::endl;
        return;
    }
    index = i;
    if(flip)
    {
        sprite.setTextureRect(sf::IntRect(((i+1) % colSize) * tileWidth, (i / colSize) * tileHeight, -tileWidth, tileHeight));
    }
    else
    {
        sprite.setTextureRect(sf::IntRect((i % colSize) * tileWidth, (i / colSize) * tileHeight, tileWidth, tileHeight));
    }
    sprite.setTexture(_texture);
}

void CUISpriteElement::render(sf::RenderWindow &window)
{
    //std::cout << "Rendering an object" << std::endl;
    //std::cout << position.x << ", " << position.y << ", " << position.z << std::endl;
    //std::cout << "Render: " << test << std::endl;
    sf::Vector2u wsize = window.getSize();
    sprite.setPosition(wsize.x * position.x - (spriteSize.x * scalar.x) * anchorPoint.x,
        wsize.y * position.y - (spriteSize.y * scalar.y) * anchorPoint.y);
    window.draw(sprite);
}