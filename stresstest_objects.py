import engine
import random

count = 1850

game = engine.Game("Stress Test: {} Objects".format(count))
game.setWindowSize(800, 600)
game.setTargetFramerate(60)


class BouncyObject(engine.Object):

    def __init__(self):
        super().__init__()
        self.direction = [1, 1]
        self.speed: float = 80
        self.collideable = False

    def update(self, f):
        pos = self.position
        if pos[0] <= 0:
            self.direction[0] = 1
        if pos[1] <= 0:
            self.direction[1] = 1
        if pos[0] + self.size[0] >= game.windowSize[0]:
            self.direction[0] = -1
        if pos[1] + self.size[1] >= game.windowSize[1]:
            self.direction[1] = -1
        self.addPosition(f * self.speed * self.direction[0],
                         f * self.speed * self.direction[1])


for _ in range(0, count):
    o = BouncyObject()
    o.loadTexture("data/spritesheets/crate.png")
    o.setSpriteSize(72, 72)
    o.setPosition(random.randint(10, 790), random.randint(10, 590))
    game.addObject(o)

ui = engine.UITextElement("arial_7.ttf")
ui.text = "Objects: {}".format(count)
ui.setPosition(0.5, 0)
ui.setAnchorPosition(0.5, 0)
ui.setOutlineColour(0, 0, 0)
ui.outlineThickness = 3
game.addUIElement(ui)

ui2 = engine.UISpriteElement()
ui2.loadTexture("data/spritesheets/reticule.png")
ui2.setAnchorPosition(1, 0)
ui2.setPosition(1, 0)
ui2.setSpriteSize(48, 48)
game.addUIElement(ui2)

game.debug = True

game.run()
