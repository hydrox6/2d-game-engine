#include "Backdrop.h"
#include <cmath>
#include <fstream>
#include <iostream>

void CBackdrop::setTileScale(float scalarX, float scalarY)
{
    sprite.setScale(scalarX, scalarY);
    tileSize.x *= scalarX;
    tileSize.y *= scalarY;
}

void CBackdrop::offsetPosition(float dx, float dy)
{
    offset = sf::Vector2f(dx, dy);
}

void CBackdrop::setWrap(bool state)
{
    wrap = state;
}

//Turns out modulo operators don't like negatives
long safemod(long a, long b)
{ 
    return (a % b + b) % b; 
}

float safefmod(float a, float b)
{ 
    return fmod((fmod(a, b) + b), b); 
}

void CBackdrop::setTex()
{
    //Workaround for SFML's buggy Sprite/Texture combo,
    //where the pointer does not update for some reason
    sprite.setTexture(texture);
}

void CBackdrop::render(sf::RenderWindow &window, int windowWidth, int windowHeight, sf::Vector2f cameraPos)
{
    //Calculate what tile should be on the left and top edges of the screen
    int leftEdge = floor((cameraPos.x - offset.x) / tileSize.x);
    int topEdge = floor((cameraPos.y - offset.y) / tileSize.y);
    
    //How much to shift the tiles left and up by
    float subX = safefmod(cameraPos.x, tileSize.x);
    float subY = safefmod(cameraPos.y, tileSize.y);

    for(int y = 0; y * tileSize.y <= windowHeight; ++y)
    {
        for(int x = 0; x * tileSize.x <= windowWidth; ++x)
        {
            int tx = leftEdge + x;
            int ty = topEdge + y;
            if(wrap)
            {
                tx = safemod(tx, tileMapSize.x);
                ty = safemod(ty, tileMapSize.y);
            }
            else if(tx >= tileMapSize.x || ty >= tileMapSize.y)
            {
                continue;
            }
            int spriteValue = tileMap.get(tx, ty);
            sprite.setTextureRect(sf::IntRect((spriteValue % countX) * rawTileSize.x, (spriteValue / countX) * rawTileSize.y, rawTileSize.x, rawTileSize.y));
            sprite.setPosition(x * tileSize.x - subX, y * tileSize.y - subY);
            window.draw(sprite);
        }
    }
}

void CBackdrop::loadTextureSheet(std::string path, int tileWidth, int tileHeight)
{
    texture.loadFromFile(path);
    sprite.setTexture(texture);
    sf::Vector2u size = texture.getSize();
    int width = size.x;
    int height = size.y;

    tileSize = sf::Vector2f(tileWidth, tileHeight);
    rawTileSize = sf::Vector2u(tileWidth, tileHeight);

    if(width <= tileWidth && height <= tileHeight)
    {
        countX = 1;
    }
    else
    {
        countX = ceil(width / tileWidth);
    }
}

void CBackdrop::loadTileMapFromFile(std::string path)
{
    std::ifstream fl;
    
    fl.open(path);
    if (!fl) 
    {
        std::cout << "Unable to load Tile Map from " << path << std::endl;
        return;
    }
    
    int w, h;

    fl >> w >> h;
    int value;

    tileMap.setSize(w, h);
    tileMapSize = sf::Vector2u(w, h);
    
    for(int y = 0; y < h; ++y)
    {
        for(int x = 0; x < w; ++x)
        {
            fl >> value;
            tileMap.set(x, y, value);
        }
    }

    fl.close();
}