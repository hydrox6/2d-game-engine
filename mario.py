import engine
from Keys import Keys
from Side import Side
from typing import List, Tuple

game = engine.Game("Super Mario Bros.")
game.setWindowSize(1024, 896)
game.tickrate = 120
game.framerate = 60
game.debug = True

sign = lambda x: x and (1, -1)[x < 0]

overseer = None


class Block(engine.Object):

    def __init__(self, x: float, y: float):
        super().__init__()
        self.setPosition(x * 64, y * 64)


class Brick(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/brick.png")
        self.setSpriteSize(64, 64)


class Floor(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/floor.png")
        self.setSpriteSize(64, 64)


class Container(Block):

    def __init__(self, x: float, y: float, extra):
        super().__init__(x, y)
        self.loadTextureSheet("data/spritesheets/mario/questionmarkblock.png", 16, 16)
        self.setTexture(0)
        self.setSpriteSize(64, 64)
        self.filled: bool = True
        self.contents = extra[0]
        self.visible: bool = True
        if len(extra) >= 2:
            self.visible = not extra[1] == "1"

        self.anim_frame: int = 0
        self.frame_count: int = 3
        self._anim_wait: float = 0.7
        self.anim_wait: float = self._anim_wait
        self.anim_direction: int = 1

    def collide(self, other: engine.Object, side: Side):
        if self.filled:
            if side == engine.Side.Bottom:
                overseer.add_score(200)
                overseer.add_coins(1)
                self.setTexture(3)
                self.filled = False

    def prerender(self, f: float):
        if self.filled:
            self.anim_wait -= f
            if self.anim_wait <= 0:
                self.anim_frame += self.anim_direction
                self.anim_wait = self._anim_wait
                if self.anim_frame >= self.frame_count - 1:
                    self.anim_direction = -1
                elif self.anim_frame <= 0:
                    self.anim_direction = 1
                self.setTexture(self.anim_frame)


class PipeHead(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/pipehead.png")
        self.setSpriteSize(128, 64)


class PipeTube(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/pipetube.png")
        self.setSpriteSize(128, 64)


class Cube(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/cube.png")
        self.setSpriteSize(64, 64)


class Flagpole(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/flagpole.png")
        self.setSpriteSize(64, 640)
        self.collideable: bool = True

    def collide(self, other: engine.Object, side: Side):
        if overseer.cutscene_state == 0 and isinstance(other, Mario):
            self.collideable = False
            other.setPosition(self.position[0] + self.size[0] / 4, other.position[1])
            overseer.end_level(self.position[0])


class Flag(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/flag.png")
        self.setSpriteSize(96, 68)
        self.collideable: bool = False


class Castle(Block):

    def __init__(self, x: float, y: float):
        super().__init__(x, y)
        self.loadTexture("data/spritesheets/mario/castle.png")
        self.setSpriteSize(320, 320)
        self.collideable: bool = False


class PointsDisplay(engine.UITextElement):

    def __init__(self, path: str):
        super().__init__()
        self.visible: bool = False
        self.setAnchorPosition(0, 1)
        self.characterSize: int = 24
        self._visibleFor: int = 1
        self.visibleFor: int = self._visibleFor
        self.text: bool = None

    def update(self, f):
        if self.visible:
            ppos = game.getObjectScreenPositon(player)
            self.setPosition(ppos[0], ppos[1] - 0.01)
            self.visibleFor -= f
            if self.visibleFor <= 0:
                self.visible = False

    def display(self, text):
        self.visibleFor = self._visibleFor
        self.text = text
        self.visible = True


class FlashingCoinUI(engine.UISpriteElement):

    def __init__(self):
        self.loadTextureSheet("data/spritesheets/mario/uicoin.png", 5, 8)
        self.setTexture(0)
        self.setSpriteSize(20, 32)
        self.anim_frame: int = 0
        self.anim_direction: int = 1
        self.frame_count: int = 3
        self._anim_wait: float = 0.7
        self.anim_wait: float = self._anim_wait

    def prerender(self, f: float):
        self.anim_wait -= f
        if self.anim_wait <= 0:
            self.anim_frame += self.anim_direction
            self.anim_wait = self._anim_wait
            if self.anim_frame >= self.frame_count - 1:
                self.anim_direction = -1
            elif self.anim_frame <= 0:
                self.anim_direction = 1
            self.setTexture(self.anim_frame)


class Overseer(engine.Object):

    def __init__(self):
        self.is_level_active: bool = False
        self.score: int = 0
        self.lives: int = 3
        self.coins: int = 0
        self.world: int = 1
        self.level: int = 1
        self.time: int = 400
        self.time_delta: float = 0
        self.cutscene_state: int = 0
        self.dummy_timer: int = 0
        self.next_level: List[int, int] = []
        self.start_pos: List[float, float] = [0, 0]
        self.end_pos: float = 0
        self.last_delta_y: float = 0

        self.points_display = PointsDisplay("data/Super Mario Bros. NES.ttf")
        game.addUIElement(self.points_display)

        self.backdrop = engine.Backdrop()
        self.tilemap_root = "data/tilemaps/mario/"
        self.sheet_root = "data/spritesheets/mario/"
        self.blockmap_root = "data/blockmaps/mario/"
        self.instructions_root = "data/instructions/mario/"

        self.blocklist = (Floor, Brick, Container, PipeHead, PipeTube, Cube, Flagpole, Flag, Castle)

        self.score_text = engine.UITextElement("data/Super Mario Bros. NES.ttf")
        self._update_score()
        self.score_text.setPosition(0.2, 0)
        self.score_text.setAnchorPosition(0.5, 0)
        game.addUIElement(self.score_text)

        self.coins_text = engine.UITextElement("data/Super Mario Bros. NES.ttf")
        self._update_coins()
        self.coins_text.setPosition(0.4, 0.034)
        self.coins_text.setAnchorPosition(0.5, 0)
        game.addUIElement(self.coins_text)

        self.level_text = engine.UITextElement("data/Super Mario Bros. NES.ttf")
        self._update_level()
        self.level_text.setPosition(0.6, 0)
        self.level_text.setAnchorPosition(0.5, 0)
        game.addUIElement(self.level_text)

        self.time_text = engine.UITextElement("data/Super Mario Bros. NES.ttf")
        self._update_time()
        self.time_text.setPosition(0.8, 0)
        self.time_text.setAnchorPosition(0.5, 0)
        game.addUIElement(self.time_text)

        self.coin_icon = FlashingCoinUI()
        self.coin_icon.setPosition(0.355, 0.035)
        self.coin_icon.setAnchorPosition(0.5, 0)
        game.addUIElement(self.coin_icon)

        self.flag: Flag = None

    def _update_score(self):
        self.score_text.text = "MARIO\n{}".format(str(self.score).rjust(6, "0"))

    def _update_coins(self):
        self.coins_text.text = " x{}".format(str(self.coins).rjust(2, "0"))

    def _update_level(self):
        self.level_text.text = "WORLD\n{}-{}".format(str(self.world).rjust(2, " "), self.level)

    def _update_time(self):
        self.time_text.text = "TIME\n{}".format(str(self.time).rjust(4))

    def set_score(self, value):
        self.score = value
        self._update_score()

    def add_score(self, value, show_overhead=True):
        self.score += value
        if show_overhead:
            self.points_display.display(str(value))
        self._update_score()

    def set_coins(self, value):
        self.coins = value
        self._update_coins()

    def add_coins(self, value):
        self.coins += value
        if self.coins >= 100:
            self.coins -= 100
            self.lives += 1
            self.points_display.display("1UP")
        self._update_coins()

    def set_world(self, value):
        self.world = value
        self._update_level()

    def set_level(self, value):
        self.level = value
        self._update_level()

    def add_time(self, value):
        self.time += value
        self._update_time()

    def set_time(self, value):
        self.time = value
        self._update_time()

    def load_level(self, world, level):
        game.clearObjects()
        game.addObject(self)
        self.world = world
        self.level = level
        name = "{}-{}".format(world, level)
        fl = open(self.instructions_root + name + ".txt", "r")
        inst = fl.read().split("\n")
        fl.close()
        self.next_level = [int(x) for x in inst[0].split(" ")]
        self.start_pos = [int(x) for x in inst[1].split(" ")]
        player.reset(*self.start_pos)
        game.setCameraPosition(0, 0)
        self.cutscene_state = 0
        self.backdrop.loadTextureSheet(self.sheet_root + name + ".png", 16, 16)
        self.backdrop.loadTileMapFromFile(self.tilemap_root + name + ".txt")
        self.backdrop.setWrap(True)
        self.backdrop.setTileScale(4, 4)
        game.setLevel(self.backdrop)

        fl = open(self.blockmap_root + name + ".txt", "r")
        blocks = fl.read().split("\n")
        fl.close()
        for block_line in blocks:
            data = block_line.split(" ")
            x = int(data[0])
            y = int(data[1])
            block_type = int(data[2])
            if len(data) >= 4:
                block = self.blocklist[block_type](x, y, data[3:])
            else:
                block = self.blocklist[block_type](x, y)
            game.addObject(block)
            if block_type == 7:
                self.flag = block

        self.is_level_active = True
        self.set_time(400)
        game.addObject(player)

    def reload_level(self):
        game.clearObjects()
        self.load_level(self.world, self.level)
        game.setCameraPosition(0, 0)
        player.reset(3, 5)
        self.set_score(0)
        self.set_coins(0)
        self.is_level_active = True

    def end_level(self, end_pos):
        player.setTexture(7)
        self.dummy_timer = 2
        self.is_level_active = False
        self.cutscene_state = 1
        self.end_pos = end_pos + 6 * 64
        player.collideable = False

    def life_lost(self):
        self.lives -= 1
        self.is_level_active = False
        player.collideable = False

    def update(self, f):
        if self.is_level_active:
            player.collideable = True
            self.time_delta += f
            if self.time_delta >= 0.4:
                self.add_time(-1)
                self.time_delta = 0
                if self.time <= 0:
                    player.life_lost()

            if game.isKeyPressed(Keys.Right):
                game.addCameraPosition(1024 * f, 0)
            if game.isKeyPressed(Keys.Left):
                game.addCameraPosition(-1024 * f, 0)
            if game.isKeyPressed(Keys.Up):
                game.addCameraPosition(0, -1024 * f)
            if game.isKeyPressed(Keys.Down):
                game.addCameraPosition(0, 1024 * f)

            if game.isKeyPressed(Keys.L):
                self.add_coins(1)

        # Slide down pole
        elif self.cutscene_state == 1:
            if self.flag.position[1] < 10 * 64 or player.position[1] < 10 * 64 - 10:
                if self.flag.position[1] < 10 * 64:
                    self.flag.addPosition(0, 192 * f)
                if player.position[1] < 10 * 64 - 10:
                    player.addPosition(0, 192 * f)
                else:
                    player.setPosition(player.position[0], 10 * 64 - 10)
            else:
                self.cutscene_state = 2
                player.setTexture(5)
                player.jump_speed = -120 * 4
                player.setPosition(player.position[0], 10 * 64 - 10)
        # Jump off pole
        elif self.cutscene_state == 2:
            if player.position[1] < 11 * 64 - 5:
                player.jump_speed += 48 * 16 * f
                player.addPosition(64 * f, player.jump_speed * f)
            else:
                player.setTexture(0)
                self.cutscene_state = 3
                player.setPosition(player.position[0], 11 * 64)
                self.last_delta_y = 0
        # Walk to castle
        elif self.cutscene_state == 3:
            if player.position[0] < self.end_pos:
                player.setTexture(((player.position[0] // 40) % 3) + 1)
                player.addPosition(192 * f, 0)
            else:
                self.cutscene_state = 4
                player.setTexture(0)
                self.dummy_timer = 0.1

        elif self.cutscene_state == 4:
            if self.time > 0:
                self.dummy_timer -= f
                if self.dummy_timer <= 0:
                    self.add_score(50, False)
                    self.add_time(-1)
            else:
                self.load_level(*self.next_level)


overseer = Overseer()
game.addObject(overseer)


class Mario(engine.Object):

    def __init__(self):
        super().__init__()
        self.health = 1
        self.maxSpeed = 70 * 6
        self.speed = 0
        self.accel = 70 * 8
        self.deccel = 84 * 8
        self.running = False
        self.jumping = False

        self.jump = 0.6
        self.remaining_jump = self.jump
        self.jump_speed = 0
        self.jumpaccel = 100 * 4

        self.last_delta_x = 0
        self.last_delta_y = 0

        self.loadTextureSheet("data/spritesheets/mario/mario.png", 16, 16)
        self.setTexture(0)
        self.setSpriteSize(64, 64)

        self.anim_locked = False
        self.anim_state = 0
        self._anim_time = 0.5
        self.anim_time = self._anim_time
        self.last_direction = 1
        self.collideable = True

    def reset(self, x, y):
        self.collideable = True
        self.health = 1
        self.speed = 0
        self.running = False

        self.remaining_jump = self.jump
        self.jump_speed = 0

        self.last_delta_x = 0
        self.last_delta_y = 0

        self.setPosition(x * 64, y * 64)
        self.setTexture(0)

        self.anim_locked = False
        self.anim_state = 0
        self.anim_time = self._anim_time
        self.last_direction = 1

    def life_lost(self):
        self.health = 0
        self.setTexture(6)
        self.jump_speed = -1000
        self.anim_time = 4
        self.anim_locked = True
        overseer.life_lost()

    def update(self, f):
        if overseer.is_level_active:
            speed_delta = 0
            
            if game.isKeyPressed(Keys.D):
                speed_delta += self.accel
                self.last_direction = 1
            if game.isKeyPressed(Keys.A):
                speed_delta -= self.accel
                self.last_direction = -1

            if speed_delta == 0:
                self.speed -= sign(self.speed) * self.deccel * f
            else:
                self.speed += speed_delta * f
                if abs(self.speed) > self.maxSpeed:
                    self.speed = sign(self.speed) * self.maxSpeed

            if speed_delta == 0 and abs(self.speed) <= 80:
                self.speed = 0

            if sign(self.speed) == sign(speed_delta) * -1:
                if abs(self.speed) <= 160:
                    self.speed = 0

            self.addPosition(self.speed * f, 0)
            self.last_delta_x = self.speed * f

            spos = game.getObjectScreenPositon(self)[0]

            if spos >= 0.45:
                game.addCameraPosition(self.speed * f, 0)
            elif spos <= 0:
                self.setPosition(game.cameraPosition[0], self.position[1])
                self.speed = 0

            # Gravity
            vaccel = 64 * 16

            if game.isKeyPressed(Keys.Space) and self.remaining_jump > 0 and self.jump_speed <= 0:
                self.jumping = True
                self.remaining_jump -= f
                self.jump_speed = -self.jumpaccel
                if not self.anim_locked:
                    self.anim_locked = True
                    self.setTexture(5, self.last_direction == -1)
            else:
                self.jump_speed += vaccel * f

            if self.remaining_jump < self.jump and not game.isKeyPressed(Keys.Space):
                self.remaining_jump = 0

            self.last_delta_y = self.jump_speed * f

            self.addPosition(0, self.jump_speed * f)

            if self.position[1] >= 14.5 * 64:
                self.life_lost()

        elif self.health == 0:
            self.jump_speed += 64 * 16 * f
            self.addPosition(0, self.jump_speed * f)
            self.anim_time -= f
            if self.anim_time <= 0:
                if overseer.lives >= 0:
                    overseer.reload_level()
                else:
                    game.close()

    def collide(self, other: engine.Object, side: Side):
        if side == Side.Left or side == Side.Right:
            self.addPosition(-self.last_delta_x, 0)
            self.speed = 0
        elif side == Side.Top or side == Side.Bottom:
            self.addPosition(0, -self.last_delta_y)

        if side == Side.Bottom:
            self.remaining_jump = self.jump
            self.jump_speed = 0
            if self.anim_locked:
                self.anim_locked = False
                self.setTexture(0, self.last_direction == -1)
        elif side == Side.Top:
            self.remaining_jump = 0
            self.jump_speed = 0
    
    def prerender(self, f):
        if not self.anim_locked:
            if abs(self.speed) < 2:
                self.setTexture(0, self.last_direction == -1)
                self.anim_state = 0
            else:
                if self.last_direction != sign(self.speed):
                    self.setTexture(4, self.last_direction == -1)
                elif abs(self.speed) > 0:
                    self.anim_time -= (abs(self.speed) / 42) * f
                    if self.anim_time <= 0:
                        self.anim_time = self._anim_time
                        self.anim_state += 1
                        if self.anim_state > 3:
                            self.anim_state -= 3
                        self.setTexture(self.anim_state, self.last_direction == -1)


player = Mario()
overseer.load_level(1, 1)

game.run()
